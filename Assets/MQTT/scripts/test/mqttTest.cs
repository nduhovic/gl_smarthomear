﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using TMPro;
using System.Globalization;

using System;
using System.Collections.Generic;


public class mqttTest : MonoBehaviour {
	private MqttClient client;
    Message receivedMessage= new Message();
    List<Message> messageHistory = new List<Message>(0);




    // Use this for initialization
    void Start () {
		// create client instance 
		client = new MqttClient(IPAddress.Parse("127.0.0.1"),1883 , false , null ); 
		
		// register to message received 
		client.MqttMsgPublishReceived += client_MqttMsgPublishReceived; 
		
		string clientId = "unity"; 
		client.Connect(clientId); 
		
		// subscribe to the all topics
		client.Subscribe(new string[] { "#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE }); 

	}
	void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
	{
        receivedMessage = new Message(e.Topic,System.Text.Encoding.UTF8.GetString(e.Message), DateTime.Now.ToString());
        messageHistory.Add(receivedMessage);
        //scrolling effect; messageHistory holds just 10 newest messages
        if (messageHistory.Count > 10)
        {
            messageHistory.RemoveAt(1);
        }
        Debug.Log("topic:" + receivedMessage.topic);
        Debug.Log("Received: " + receivedMessage.content);
    }

    void OnGUI()
    {
        //publish button
        if (GUI.Button(new Rect(20, 40, 80, 20), "Publish"))
        {
            Debug.Log("sending...");
            client.Publish("test_topic", System.Text.Encoding.UTF8.GetBytes("test publish"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
            Debug.Log("sent");
        }

        //change value of text mesh object found by name to message received
        TextMeshPro textObject = GameObject.Find("temp_text").GetComponent<TextMeshPro>();
        if (receivedMessage.topic.Equals("temp")) { textObject.SetText(receivedMessage.content); }

        //add textbox with all messages received
        GUI.backgroundColor = Color.gray;
        //GUI.Box(new Rect(0, (3 * Screen.height / 4), Screen.width, Screen.height / 4));
        //for (int i = 0; i < messageHistory.Length; i++)
            GUI.Box(new Rect(5, (3 * Screen.height / 4)-35, Screen.width-10, Screen.height / 4+9), "Readings:");
        for (int i = 0; i < messageHistory.Count; i++)
        {
            GUI.Label(new Rect(5, (3 * Screen.height / 4+12*i)-20, Screen.width, Screen.height / 4), messageHistory[i].timeStamp + "\tSensorID: " + messageHistory[i].topic + "\tReading: " + messageHistory[i].content);
        
    }


    }
	// Update is called once per frame
	void Update () {
        

        }
}
public struct Message
{
    public String topic, content, timeStamp;

    public Message(String topic, String content, string timeStamp)
    {
        this.topic = topic;
        this.content = content;
        this.timeStamp = timeStamp;

    }
}
